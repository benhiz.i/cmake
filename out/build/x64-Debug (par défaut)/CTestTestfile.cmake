# CMake generated Testfile for 
# Source directory: M:/Development/cmake/dev
# Build directory: M:/Development/cmake/dev/out/build/x64-Debug (par défaut)
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
include("M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/Tests/EnforceConfig.cmake")
add_test(SystemInformationNew "M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/bin/cmake" "--system-information" "-G" "Ninja")
set_tests_properties(SystemInformationNew PROPERTIES  _BACKTRACE_TRIPLES "M:/Development/cmake/dev/CMakeLists.txt;853;add_test;M:/Development/cmake/dev/CMakeLists.txt;0;")
subdirs("Source/kwsys")
subdirs("Utilities/std")
subdirs("Utilities/KWIML")
subdirs("Utilities/cmlibrhash")
subdirs("Utilities/cmzlib")
subdirs("Utilities/cmcurl")
subdirs("Utilities/cmnghttp2")
subdirs("Utilities/cmexpat")
subdirs("Utilities/cmbzip2")
subdirs("Utilities/cmzstd")
subdirs("Utilities/cmliblzma")
subdirs("Utilities/cmlibarchive")
subdirs("Utilities/cmjsoncpp")
subdirs("Utilities/cmlibuv")
subdirs("Source")
subdirs("Utilities")
subdirs("Tests")
subdirs("Auxiliary")

cmake_minimum_required(VERSION 2.8.10)

set(CTEST_SOURCE_DIRECTORY "M:/Development/cmake/dev/Tests/VSProjectInSubdir")
set(CTEST_BINARY_DIRECTORY "M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/Tests/CTestBuildCommandProjectInSubdir/Nested")
set(CTEST_CMAKE_GENERATOR "Ninja")
set(CTEST_BUILD_CONFIGURATION "Debug")

ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY})
ctest_start(Experimental)
ctest_configure(OPTIONS "-DCMAKE_MAKE_PROGRAM:FILEPATH=C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/Common7/IDE/CommonExtensions/Microsoft/CMake/Ninja/ninja.exe")
ctest_build(TARGET test)

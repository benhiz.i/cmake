# CMake generated Testfile for 
# Source directory: M:/Development/cmake/dev/Tests/FindThreads
# Build directory: M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/Tests/FindThreads
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(FindThreads.C-only "M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/bin/ctest" "--build-and-test" "M:/Development/cmake/dev/Tests/FindThreads/C-only" "M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/Tests/FindThreads/C-only" "--build-generator" "Ninja" "--build-makeprogram" "C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/Common7/IDE/CommonExtensions/Microsoft/CMake/Ninja/ninja.exe" "--build-project" "FindThreads_C-only" "--build-options" "--test-command" "M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/bin/ctest" "-V")
set_tests_properties(FindThreads.C-only PROPERTIES  _BACKTRACE_TRIPLES "M:/Development/cmake/dev/Tests/FindThreads/CMakeLists.txt;2;add_test;M:/Development/cmake/dev/Tests/FindThreads/CMakeLists.txt;0;")
add_test(FindThreads.CXX-only "M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/bin/ctest" "--build-and-test" "M:/Development/cmake/dev/Tests/FindThreads/CXX-only" "M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/Tests/FindThreads/CXX-only" "--build-generator" "Ninja" "--build-makeprogram" "C:/Program Files (x86)/Microsoft Visual Studio/2019/Community/Common7/IDE/CommonExtensions/Microsoft/CMake/Ninja/ninja.exe" "--build-project" "FindThreads_CXX-only" "--build-options" "--test-command" "M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/bin/ctest" "-V")
set_tests_properties(FindThreads.CXX-only PROPERTIES  _BACKTRACE_TRIPLES "M:/Development/cmake/dev/Tests/FindThreads/CMakeLists.txt;2;add_test;M:/Development/cmake/dev/Tests/FindThreads/CMakeLists.txt;0;")

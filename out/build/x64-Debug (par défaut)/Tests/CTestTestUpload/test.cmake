cmake_minimum_required(VERSION 2.4)

# Settings:
set(CTEST_DASHBOARD_ROOT                "M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/Tests/CTestTest")
set(CTEST_SITE                          "IORIKYU-PC")
set(CTEST_BUILD_NAME                    "CTestTest-Win32-ninja-Upload")

set(CTEST_SOURCE_DIRECTORY              "M:/Development/cmake/dev/Tests/CTestTestUpload")
set(CTEST_BINARY_DIRECTORY              "M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/Tests/CTestTestUpload")
set(CTEST_CMAKE_GENERATOR               "Ninja")
set(CTEST_CMAKE_GENERATOR_PLATFORM      "")
set(CTEST_CMAKE_GENERATOR_TOOLSET       "")
set(CTEST_BUILD_CONFIGURATION           "$ENV{CMAKE_CONFIG_TYPE}")

CTEST_START(Experimental)
CTEST_CONFIGURE(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE res)
CTEST_BUILD(BUILD "${CTEST_BINARY_DIRECTORY}" RETURN_VALUE res)
CTEST_UPLOAD(FILES "${CTEST_SOURCE_DIRECTORY}/sleep.c" "${CTEST_BINARY_DIRECTORY}/CMakeCache.txt")
CTEST_SUBMIT()

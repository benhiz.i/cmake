if(NOT "M:/Development/cmake/dev/Tests/CMakeTests" MATCHES "^/")
  set(slash /)
endif()
set(url "file://${slash}M:/Development/cmake/dev/Tests/CMakeTests/FileDownloadInput.png")
set(dir "M:/Development/cmake/dev/out/build/x64-Debug (par défaut)/Tests/CMakeTests/downloads")

file(DOWNLOAD
  ${url}
  ${dir}/file3.png
  TIMEOUT 2
  STATUS status
  EXPECTED_HASH SHA1=5555555555555555555555555555555555555555
  )
